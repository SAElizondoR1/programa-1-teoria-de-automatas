package automatas;

import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String entrada; // cadena de entrada.

        do {
            System.out.println("Introduzca una cadena de caracteres: ");
            entrada = sc.nextLine();

            if (esValidaLR(entrada))
                System.out.println("La cadena es válida para el lenguaje regular.");
            else
                System.out.println("La cadena no es válida para el lenguaje regular.");
        } while (analizarOtra());
    }

    public static boolean analizarOtra() {
        System.out.println("¿Desea analizar otra cadena? (S - Sí, n - No)");
        String sn = sc.nextLine();
        if (sn.isEmpty())
            return true;
        return sn.charAt(0) != 'n';
    }

    public static boolean esValidaLR(String entrada) {
        String matricula = "1907852";   // Matrícula: 1907852
        String letras = "sergioandlzu";    // Nombre: Sergio Andrés Elizondo Rodríguez
        String alfabeto = "[" + matricula + letras + ".]"; // Todas las letras, dígitos y el punto.

        String cond1 = "[" + matricula + "]"; // que el primer símbolo sea un dígito válido
        String cond2 = alfabeto + "*?"; // que tenga cualquier combinación válida de letras y dígitos intermedia
        String cond3 = "saer"; // que la cadena contenga mis iniciales en forma consecutivas al menos una vez
        String cond4 = "." + matricula + "$"; // que la cadena contenga como últimos símbolos .(matrícula)
        String cond5 = "^(?!.*?\\.{2})";// que no se acepten puntos consecutivos.

        String expresion = cond5 + cond1 + cond2 + cond3 + cond2 + cond4;
        // [dígito][alfabeto]*saer[alfabeto]*\.1907852, tal que !(.*?\.\.)

        return entrada.matches(expresion);
    }
}
